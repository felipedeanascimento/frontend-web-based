import { Component } from '@angular/core';

import { Chart } from 'chart.js';

import { Router } from '@angular/router';

import { PessoaService } from '../../services/pessoa.service';

import { Pessoa } from '../../services/pessoa';

import { Response } from '../../services/response';

@Component({
    selector: 'app-relatorio-pessoas',
    templateUrl: './relatorio.component.html',
    styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent {

  chart = new Chart('canvas', {
    type: 'line',
    data: {
      labels: weatherDates,
      datasets: [
        { 
          data: temp_max,
          borderColor: "#3cba9f",
          fill: false
        },
        { 
          data: temp_min,
          borderColor: "#ffcc00",
          fill: false
        },
      ]
    },
    options: {
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          display: true
        }],
        yAxes: [{
          display: true
        }],
      }
    }
  });
}
